import com.google.gson.Gson;

public class App {
    public static void main(String[] args) {
        // source: http://tutorials.jenkov.com/java-json/gson.html
        String json = "{\"brand\":\"Jeep\", \"doors\": 3}";
        Gson gson = new Gson();
        Car car = gson.fromJson(json, Car.class);
        System.out.println(car);
    }

    static public class Car {
        public String brand = null;
        public int doors = 0;

        @Override
        public String toString() {
            return "Car{" +
                    "brand='" + brand + '\'' +
                    ", doors=" + doors +
                    '}';
        }
    }
}
