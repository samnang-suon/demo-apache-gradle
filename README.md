# How To Create An Executable JAR
## Pre-requisites
* Java JDK
* JAVA_HOME environment variable
* Gradle

## Steps
1. Update your 'build.gradle' file with:

```groovy
jar {
    manifest {
        attributes (
                'Class-Path': configurations.compile.collect { it.getName() }.join(' '),
                'Main-Class': 'App'
        )
    }
    from {
        configurations.compile.collect { it.isDirectory() ? it : zipTree(it) }
    }
}
```

**NOTE**

Without above code snippet, you will encounter this error message:

        no main manifest attribute, in <MyJar.jar>

Also, for 'Main-Class', it could be my.package.path.JavaClassName like so:

      'Main-Class': 'com.company.model.HelloWorld'

2. Package your application:

        gradle clean jar

Your JAR will be stored inside 'build/lib':

![BuildLibJar](images/BuildLibJar.png "BuildLibJar")

3. Run your application:

SYNTAX:

      java -jar <GeneratedJarFile>

EXAMPLE:

      java -jar .\build\libs\DemoApacheGradle-1.0-SNAPSHOT.jar

![RunJavaJar](images/RunJavaJar.png "RunJavaJar")

## Error 'no main manifest attribute'
If you see:

    no main manifest attribute

![NoMainManifestAttribute](images/NoMainManifestAttribute.png "NoMainManifestAttribute")

See: https://stackoverflow.com/questions/32567167/gradle-no-main-manifest-attribute

## Error 'Exception in thread "main" java.lang.NoClassDefFoundError'
If you see:

    Exception in thread "main" java.lang.NoClassDefFoundError

![NoClassDefFoundError](images/NoClassDefFoundError.png "NoClassDefFoundError")

See: https://stackoverflow.com/questions/49114170/java-gradle-build-noclassdeffounderror
